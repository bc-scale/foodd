resource "aws_s3_bucket" "this" {
  bucket = module.label.id
  acl    = "private"

  tags = module.label.tags
}